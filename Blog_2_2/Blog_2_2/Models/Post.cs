﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("Post")]
    public class Post
    {
        public int ID { set; get; }

        [Required]
        [StringLength(500,ErrorMessage="Nhập từ 20-500 ký tự"),MinLength(20,ErrorMessage="Nhập từ 20-500 ký tự")]
        public String Title { set; get; }

        [Required]
        [MinLength(50,ErrorMessage="Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required]
        [DataType(DataType.DateTime,ErrorMessage="Nhập ngày tháng DD/MM/YYYY")]
        public DateTime DateCreated { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày tháng DD/MM/YYYY")]
        public DateTime DateUpdated { set; get; }
        public int AccountID { set; get; }

        //Tạo quan hệ với comment
        public virtual ICollection<Comment> Comments { set; get; }
        //Tạo quan hệ với Account
        
        public virtual Account Account { set; get; }
        //Tạo quan hệ với tag
        public virtual ICollection<Tag> Tags { set; get; }
    }
}