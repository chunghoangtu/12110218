﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Account
    {
        //[Required]
        public int AccountID { set; get; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required]
        [DataType(DataType.EmailAddress,ErrorMessage="Nhập email hợp lệ")]
        public String Email { set; get; }

        [Required]
        [StringLength(100,ErrorMessage="Nhập tối đa 100 ký tự")]
        public String FirstName { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 ký tự")]
        public String LastName { set; get; }

        //Tạo quan hệ với Post
        public virtual ICollection<Post> Posts { set; get; }
    }
}