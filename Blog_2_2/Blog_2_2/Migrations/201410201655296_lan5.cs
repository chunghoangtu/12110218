namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Accounts", "Post_ID", "dbo.Post");
            DropIndex("dbo.Accounts", new[] { "Post_ID" });
            AddColumn("dbo.Post", "AccountID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Post", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Post", "AccountID");
            DropColumn("dbo.Accounts", "Post_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts", "Post_ID", c => c.Int());
            DropIndex("dbo.Post", new[] { "AccountID" });
            DropForeignKey("dbo.Post", "AccountID", "dbo.Accounts");
            DropColumn("dbo.Post", "AccountID");
            CreateIndex("dbo.Accounts", "Post_ID");
            AddForeignKey("dbo.Accounts", "Post_ID", "dbo.Post", "ID");
        }
    }
}
