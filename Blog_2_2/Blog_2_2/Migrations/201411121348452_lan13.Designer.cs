// <auto-generated />
namespace Blog_2_2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan13 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan13));
        
        string IMigrationMetadata.Id
        {
            get { return "201411121348452_lan13"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
