namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Post_ID = c.Int(),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.Post", t => t.Post_ID)
                .Index(t => t.Post_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Accounts", new[] { "Post_ID" });
            DropForeignKey("dbo.Accounts", "Post_ID", "dbo.Post");
            DropTable("dbo.Accounts");
        }
    }
}
