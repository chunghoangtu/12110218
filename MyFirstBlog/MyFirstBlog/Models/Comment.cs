﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstBlog.Models
{
    public class Comment
    {
        [Key]
        public int ID { set; get; }
        public int Post_ID { set; get; }
        public String Body { set; get; }
    }
}