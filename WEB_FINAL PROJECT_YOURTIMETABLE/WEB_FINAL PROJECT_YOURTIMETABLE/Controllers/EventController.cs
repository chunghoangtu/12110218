﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEB_FINAL_PROJECT_YOURTIMETABLE.Models;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Controllers
{
    public class EventController : Controller
    {
        private YOURTIMETABLEDbContext db = new YOURTIMETABLEDbContext();

        //
        // GET: /Event/

        public ActionResult Index()
        {
            var events = db.Events.Include(e => e.UserProfile);
            return View(events.ToList());
        }

        //
        // GET: /Event/Details/5

        public ActionResult Details(int id = 0)
        {
            Event myevent = db.Events.Find(id);
            if (myevent == null)
            {
                return HttpNotFound();
            }
            return View(myevent);
        }

        //
        // GET: /Event/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Event/Create

        [HttpPost]
        public ActionResult Create(Event myevent)
        {
            if (ModelState.IsValid)
            {
                db.Events.Add(myevent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", myevent.UserProfileUserId);
            return View(myevent);
        }

        //
        // GET: /Event/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Event myevent = db.Events.Find(id);
            if (myevent == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", myevent.UserProfileUserId);
            return View(myevent);
        }

        //
        // POST: /Event/Edit/5

        [HttpPost]
        public ActionResult Edit(Event myevent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(myevent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", myevent.UserProfileUserId);
            return View(myevent);
        }

        //
        // GET: /Event/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Event myevent = db.Events.Find(id);
            if (myevent == null)
            {
                return HttpNotFound();
            }
            return View(myevent);
        }

        //
        // POST: /Event/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Event myevent = db.Events.Find(id);
            db.Events.Remove(myevent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}