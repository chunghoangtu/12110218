﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEB_FINAL_PROJECT_YOURTIMETABLE.Models;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Controllers
{
    public class AdvertisingController : Controller
    {
        private YOURTIMETABLEDbContext db = new YOURTIMETABLEDbContext();

        //
        // GET: /Advertising/

        public ActionResult Index()
        {
            return View(db.Advertisings.ToList());
        }

        //
        // GET: /Advertising/Details/5

        public ActionResult Details(int id = 0)
        {
            Advertising advertising = db.Advertisings.Find(id);
            if (advertising == null)
            {
                return HttpNotFound();
            }
            return View(advertising);
        }

        //
        // GET: /Advertising/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Advertising/Create

        [HttpPost]
        public ActionResult Create(Advertising advertising)
        {
            if (ModelState.IsValid)
            {
                db.Advertisings.Add(advertising);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(advertising);
        }

        //
        // GET: /Advertising/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Advertising advertising = db.Advertisings.Find(id);
            if (advertising == null)
            {
                return HttpNotFound();
            }
            return View(advertising);
        }

        //
        // POST: /Advertising/Edit/5

        [HttpPost]
        public ActionResult Edit(Advertising advertising)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertising).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(advertising);
        }

        //
        // GET: /Advertising/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Advertising advertising = db.Advertisings.Find(id);
            if (advertising == null)
            {
                return HttpNotFound();
            }
            return View(advertising);
        }

        //
        // POST: /Advertising/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertising advertising = db.Advertisings.Find(id);
            db.Advertisings.Remove(advertising);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}