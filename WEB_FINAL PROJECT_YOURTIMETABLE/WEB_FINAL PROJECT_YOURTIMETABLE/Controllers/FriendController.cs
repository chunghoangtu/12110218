﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEB_FINAL_PROJECT_YOURTIMETABLE.Models;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Controllers
{
    public class FriendController : Controller
    {
        private YOURTIMETABLEDbContext db = new YOURTIMETABLEDbContext();

        //
        // GET: /Friend/

        public ActionResult Index()
        {
            var friends = db.Friends.Include(f => f.UserProfile);
            return View(friends.ToList());
        }

        //
        // GET: /Friend/Details/5

        public ActionResult Details(int id = 0)
        {
            Friend friend = db.Friends.Find(id);
            if (friend == null)
            {
                return HttpNotFound();
            }
            return View(friend);
        }

        //
        // GET: /Friend/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Friend/Create

        [HttpPost]
        public ActionResult Create(Friend friend)
        {
            if (ModelState.IsValid)
            {
                db.Friends.Add(friend);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", friend.UserProfileUserId);
            return View(friend);
        }

        //
        // GET: /Friend/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Friend friend = db.Friends.Find(id);
            if (friend == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", friend.UserProfileUserId);
            return View(friend);
        }

        //
        // POST: /Friend/Edit/5

        [HttpPost]
        public ActionResult Edit(Friend friend)
        {
            if (ModelState.IsValid)
            {
                db.Entry(friend).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", friend.UserProfileUserId);
            return View(friend);
        }

        //
        // GET: /Friend/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Friend friend = db.Friends.Find(id);
            if (friend == null)
            {
                return HttpNotFound();
            }
            return View(friend);
        }

        //
        // POST: /Friend/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Friend friend = db.Friends.Find(id);
            db.Friends.Remove(friend);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}