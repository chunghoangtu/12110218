﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEB_FINAL_PROJECT_YOURTIMETABLE.Models;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Controllers
{
    public class TimetableController : Controller
    {
        private YOURTIMETABLEDbContext db = new YOURTIMETABLEDbContext();

        //
        // GET: /Timetable/

        public ActionResult Index()
        {
            var timetables = db.Timetables.Include(t => t.UserProfile);
            return View(timetables.ToList());
        }

        //
        // GET: /Timetable/Details/5

        public ActionResult Details(int id = 0)
        {
            Timetable timetable = db.Timetables.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            return View(timetable);
        }

        //
        // GET: /Timetable/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Timetable/Create

        [HttpPost]
        public ActionResult Create(Timetable timetable)
        {
            if (ModelState.IsValid)
            {
                db.Timetables.Add(timetable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", timetable.UserProfileUserId);
            return View(timetable);
        }

        //
        // GET: /Timetable/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Timetable timetable = db.Timetables.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", timetable.UserProfileUserId);
            return View(timetable);
        }

        //
        // POST: /Timetable/Edit/5

        [HttpPost]
        public ActionResult Edit(Timetable timetable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(timetable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", timetable.UserProfileUserId);
            return View(timetable);
        }

        //
        // GET: /Timetable/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Timetable timetable = db.Timetables.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            return View(timetable);
        }

        //
        // POST: /Timetable/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Timetable timetable = db.Timetables.Find(id);
            db.Timetables.Remove(timetable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}