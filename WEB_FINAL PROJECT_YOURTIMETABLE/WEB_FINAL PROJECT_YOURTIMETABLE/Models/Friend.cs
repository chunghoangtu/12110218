﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Models
{
    public class Friend
    {
        public int ID { set; get; }
        public String UsernameOfFriend { set; get; }

        //User
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
    }
}