﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Models
{
    public class Event
    {
        public int ID { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Chọn thời gian cho sự kiện")]
        public DateTime DayOfEvent { set; get; }

        [Required]
        [StringLength(500, ErrorMessage = "Nhập nội dung cho sự kiện", MinimumLength = 5)]
        public String Content { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập nội dung chuẩn bị", MinimumLength = 5)]
        public String Prepare { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập địa điểm cho sự kiện", MinimumLength = 5)]
        public String Place { set; get; }

        //User
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
    }
}