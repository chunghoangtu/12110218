﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Models
{
    public class Job
    {
        public int ID { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập thời gian cho công việc")]
        public DateTime TimeOfJob { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập địa điểm cho công việc", MinimumLength = 10)]
        public String Place { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập nội dung cho công việc", MinimumLength = 10)]
        public String Content { set; get; }

        public Boolean Done { set; get; }

        //Timetbale
        public int TimetableID { set; get; }
        public virtual Timetable Timetable { set; get; }
    }
}