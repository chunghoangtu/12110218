﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Models
{
    public class Timetable
    {
        public int ID { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập thời gian cho thời khóa biểu")]
        public DateTime DayOfTimetable { set; get; }

        public int NumberOfJob { set; get; }
        public int Completion { set; get; }
        public int Score { set; get; }

        //User
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        //Job
        public virtual ICollection<Job> Jobs { set; get; }
    }
}