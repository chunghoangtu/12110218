﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Models
{
    public class Advertising
    {
        public int ID { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập tên của quảng cáo")]
        public String Advname { set; get; }

        public String Position { set; get; }
    }
}