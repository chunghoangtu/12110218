namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DayOfEvent = c.DateTime(nullable: false),
                        Content = c.String(nullable: false, maxLength: 500),
                        Prepare = c.String(nullable: false, maxLength: 100),
                        Place = c.String(nullable: false, maxLength: 100),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Advertisings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Advname = c.String(nullable: false),
                        Position = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UsernameOfFriend = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Friends", new[] { "UserProfileUserId" });
            DropIndex("dbo.Events", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Friends", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Events", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.Friends");
            DropTable("dbo.Advertisings");
            DropTable("dbo.Events");
        }
    }
}
