namespace WEB_FINAL_PROJECT_YOURTIMETABLE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Timetables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DayOfTimetable = c.DateTime(nullable: false),
                        NumberOfJob = c.Int(nullable: false),
                        Completion = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TimeOfJob = c.DateTime(nullable: false),
                        Place = c.String(nullable: false, maxLength: 100),
                        Content = c.String(nullable: false, maxLength: 100),
                        Done = c.Boolean(nullable: false),
                        TimetableID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Timetables", t => t.TimetableID, cascadeDelete: true)
                .Index(t => t.TimetableID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Jobs", new[] { "TimetableID" });
            DropIndex("dbo.Timetables", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Jobs", "TimetableID", "dbo.Timetables");
            DropForeignKey("dbo.Timetables", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.Jobs");
            DropTable("dbo.Timetables");
            DropTable("dbo.UserProfile");
        }
    }
}
